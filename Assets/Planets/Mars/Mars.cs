﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Game;

public class Mars : MonoBehaviour {

	public Material[] matrials;
	private Renderer terrain;

	void Awake(){
		DataManager.SetTerrain (this);
		terrain = GetComponent<Renderer>();
	}

	// Use this for initialization
	void Start () {
		setMaterial (ApplicationModel.currentLevel);
	}

	public void setMaterial(int indexMat) {
		terrain.material = matrials[indexMat];
	}
	
	// Update is called once per frame
	void Update () {
		transform.Rotate(Vector3.right * Time.deltaTime);
	}
}
