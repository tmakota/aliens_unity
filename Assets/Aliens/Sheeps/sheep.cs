﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class sheep : MonoBehaviour {

	private bool shouldRender = true;
	public string unitName;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		transform.Rotate(Vector3.up * (Time.deltaTime * 5));
	}

	public void toggleRender(bool visible) {
		shouldRender = visible;

		Renderer[] renderers = gameObject.GetComponentsInChildren<Renderer>();
		foreach (Renderer r in renderers)
		{
			r.enabled = shouldRender;
		}
	}
}
