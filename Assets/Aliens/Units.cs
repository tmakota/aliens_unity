﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Game;

public class Units : MonoBehaviour {

	public GameObject[] units;
	public GameObject ufo;

	// all units in quee
	private string[] unitQuee = new string[DataManager.aliensCount];

	private Manager gameManager;
	private bool waveAdded;

	void Awake() {
		DataManager.SetGameObjectList(this);
		gameManager = transform.parent.GetComponent<Manager>();
	}

	// Use this for initialization
	void Start () {
		initGame ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void initGame(){
		waveAdded = false;
		generateAllUnits ();
	}

	public GameObject GetUfoUnit() {
		return ufo;
	}

	public void moveDown(int wave) {
			waveAdded = false;
			Alien[] units = getLiveAliens();
			foreach (Alien u in units)
			{
				u.shouldGoDown ();
			}
	}

	public Alien[] getLiveAliens(){
		return gameObject.GetComponentsInChildren<Alien>();
	}

	public GameObject GetUnitByType(string name){
		for(int i = 0; i < units.Length; i++) {
			Humanoid unit = units[i].GetComponent< Humanoid >();
			if(unit && unit.alienType == name) return units[i];
		}
		return null;
	}

	public void addUnitToList(GameObject alien) {
		alien.transform.parent = transform;
	}

	public void downDone(int wave) {
		if (wave < DataManager.waveCount && !waveAdded) {
			waveAdded = true;
			generateWaveUnit (wave);
		} else {
			DataManager.rotateFild ();
		}

	}

	private void generateWaveUnit(int wave) {
		string unitName;

		for(int i = 0; i < DataManager.waveSize; i++) {
			int index = (wave * DataManager.waveSize) + i;
			unitName = unitQuee[index];
			gameManager.addItem (unitName, i);
		}
		DataManager.rotateFild ();
	}

	public bool isUnitUnder(int row, int col){
		bool isFree = true;
		Alien[] units = getLiveAliens();
		foreach (Alien u in units)
		{
			int unitRow = u.getTopPosition;
			int unitCol = u.getColPosition;
			if (unitCol == col && unitRow == row) {
				isFree = false;
			}
		}
		return isFree;
	}

	public void removeAll(){
		Alien[] units = getLiveAliens();
		foreach (Alien u in units)
		{
			Destroy (u.transform.gameObject);
		}
	}

	void reshuffle(string[] texts)
	{
		// Knuth shuffle algorithm :: courtesy of Wikipedia :)
		for (int t = 0; t < texts.Length; t++ )
		{
			string tmp = texts[t];
			int r = Random.Range(t, texts.Length);
			texts[t] = texts[r];
			texts[r] = tmp;
		}
	}

	public void generateAllUnits() {

		string[] level = DataManager.getLevel();

		string unitName;

		for(int i = 0; i < DataManager.aliensCount;) {
			unitName = level.RandomItem();
			unitQuee[i] = unitName;
			i += 1;
			unitQuee[i] = unitName;
			i += 1;
		}
		reshuffle (unitQuee);
		generateWaveUnit (0);
	}
}

public static class ArrayExtensions
{

	public static T RandomItem<T>(this T[] array)
	{
		return array[Random.Range(0, array.Length)];
	}
		
}


