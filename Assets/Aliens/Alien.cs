﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Game;

public class Alien : MonoBehaviour {

	public string unitName;
	public float moveSpeed;

	public string alienType;
	private bool isSheepVisible;
	private bool isMoving;
	private GameObject humanoid;
	private bool isSelected;

	private float nextRow = 0.0f;
	private float originRow = 0.0f;
	private Vector3 destination;

	private int rowFromTop = 0;
	private int column;

	public int getTopPosition { get { return rowFromTop; } }
	public int getColPosition { get { return column; } }
	public bool selected { get { return isSelected; } }


	// Use this for initialization
	void Start () {
		isSheepVisible = true;
		isMoving = false;
		isSelected = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (isMoving) {
			transform.position = Vector3.MoveTowards(transform.position, destination, Time.deltaTime * moveSpeed);
			if (transform.position == destination) {
				isMoving = false;
				originRow = nextRow;
				DataManager.movingComplete ();
			}
		}
	}

	public void shouldGoDown() {
		if (rowFromTop < (DataManager.startTopPoint - 1)) {
			bool isFreeFordward = DataManager.isForwardOccupied (rowFromTop, column);
			if (isFreeFordward) {
				goDown ();
			} 
		} else {
			atack ();
		}
	}

	private void atack(){
		DataManager.decreaseLive ();
	}

	public void goDown(){
		nextRow = originRow - DataManager.unitHeight;
		destination = new Vector3(transform.position.x, nextRow, transform.position.z);
		rowFromTop++;
		isMoving = true;
	}

	public void setRow(int row) {
		originRow = (row * 1.0f) + DataManager.startTopPoint;
		nextRow = originRow;
	}

	public void setColumn(int col){
		column = col;
	}

	public void setType(string typeName) {
		alienType = typeName;
	}

	public void handleClick() {
		isSelected = true;
		isSheepVisible = !isSheepVisible;
		toggleHumanoidVisible ();
		toggleSheepVisible ();
	}

	public void hideHumanoid() {
		isSheepVisible = true;
		toggleSheepVisible ();
		Destroy (humanoid);
		isSelected = false;
	}

	private void toggleHumanoidVisible () {
		GameObject prefab = DataManager.GetHumanoid (alienType);
		Vector3 position = new Vector3 (0, 0, 0);
		humanoid = Instantiate (prefab, position, prefab.transform.rotation);
		humanoid.transform.parent = transform;
		humanoid.transform.position = transform.position;

		DataManager.addSelectedObject (transform.gameObject);
	}

	private void toggleSheepVisible () {
		sheep alienSheep = gameObject.GetComponentInChildren<sheep>();
		alienSheep.toggleRender (isSheepVisible);
	}


}