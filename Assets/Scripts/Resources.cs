﻿using UnityEngine;
using System.Collections;

namespace Game {

	public class ApplicationModel
	{
		static public int currentLevel = 0;
	}

	public static class DataManager {

		public static int startTopPoint = 8;
		public static float leftOffcet = 1.5f;
		public static float unitSpace = 1.0f;
		public static float unitHeight = 1.0f;
		public static int aliensCount = 16;
		public static int waveSize = 4;
		public static int waveCount = 4;

		public static int lives = 20;

		public static Vector3 InvalidPosition = new Vector3(-9999, -9999, -9999);
		public static float zoomSpeed = 15.0f;
		public static float panSpeed = 15.0f;
		public static float rotateSpeed = 10.0f;
		public static float waitTime = 0.1f;
		public static float lowSplit = 0.35f;
		public static float highSplit = 0.65f;
		public static int edgeBounds = 35;
		private static int padding = 10;
		private static int size = 50;
		public static Rect leftZone { get { return new Rect(edgeBounds, Screen.height - edgeBounds - padding - 2 * size, size, size); } }
		public static Rect rightZone { get { return new Rect(edgeBounds + 2 * padding + 2 * size, Screen.height - edgeBounds - padding - 2 * size, size, size); } }
		public static Rect bottomZone { get { return new Rect(edgeBounds + padding + size, Screen.height - edgeBounds - size, size, size); } }
		public static Rect topZone { get { return new Rect(edgeBounds + padding + size, Screen.height - edgeBounds - 2 * padding - 3 * size, size, size); } }

		private static Units gameUnitList;
		private static Manager gameManager;
		private static UserInput userInput;
		private static Mars planet;
		private static Bg sky;

		public static void SetTerrain(Mars terrain) {
			planet = terrain;
		}

		public static void SetSky(Bg bgSky) {
			sky = bgSky;
		}

		public static void SetGameObjectList(Units objectList) {
			gameUnitList = objectList;
		}

		public static void SetGameManager(Manager manager) {
			gameManager = manager;
		}

		public static void setInputsController(UserInput inputController){
			userInput = inputController;
		}

		public static void inputsOn(){
			userInput.releaseInput ();
		}

		public static void inputsOff(){
			userInput.disableInput ();
		}

		public static void addSelectedObject(GameObject ufo) {
			gameManager.selectHumanoid (ufo);
		}

		public static bool isGameRuning() {
			return gameManager.getGameState();
		}

		public static string[] getLevel(){
			return gameManager.getLevel ();
		}

		public static void decreaseLive(){
			gameManager.decreaseLives ();
			movingComplete ();
		}

		public static void gameOver(){
			gameUnitList.removeAll ();
		}

		public static void startNextLevel(){
			sky.setMaterial (gameManager.getLevelNum());
			planet.setMaterial (gameManager.getLevelNum());
			gameUnitList.generateAllUnits ();
		}

		public static int getAliensCount(){
			return gameUnitList.getLiveAliens ().Length;
		}

		public static void movingComplete(){
			gameUnitList.downDone (gameManager.getWave());
		}

		public static GameObject GetUfoUnit() {
			return gameUnitList.GetUfoUnit();
		}

		public static void addUnitToList(GameObject alien) {
			gameUnitList.addUnitToList (alien);
		}

		public static GameObject GetHumanoid(string name) {
			return gameUnitList.GetUnitByType(name);
		}

		public static bool isForwardOccupied(int row, int col){
			return gameUnitList.isUnitUnder (row + 1, col);
		}

		public static void moveAllDown() {
			gameUnitList.moveDown (gameManager.getWave());
		}

		public static void restart(){
			gameManager.restartGame ();
			gameUnitList.generateAllUnits ();
			sky.setMaterial (0);
			planet.setMaterial (0);
		}

		public static void rotateFild(){
			gameManager.rotate();
		}
	}
}
