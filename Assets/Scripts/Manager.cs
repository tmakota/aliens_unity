﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

using Game;

public class Manager : MonoBehaviour {

	private int openItems;
	private GameObject[] selected = new GameObject[2];
	private string currentType;
	private int wave;
	private int level;
	private int score;
	private string[][] levels = new string[4][];
	private int currentLives;
	private bool gameRun;
	private bool isRotated = false;

	private float ROTATE_TIME = 3.0f; 
	private float ROTATION_MAX = 180.0f; 
	private float timer = 0.0f;
	private int menuImageSide;

	void Awake() {
		initLevels ();
		DataManager.SetGameManager(this);
	}

	// Use this for initialization
	void Start () {
		gameRun = true;
		resetAllData ();
		menuImageSide = Screen.height / 4;
	}

	// Update is called once per frame
	void Update () {
		if (isRotated) {
			timer += Time.deltaTime;
			if (timer < ROTATE_TIME) {
				gameObject.transform.Rotate (Vector3.up * Time.deltaTime * ROTATION_MAX / ROTATE_TIME);
			} else {
				isRotated = false;
				timer = 0.0f;
				DataManager.inputsOn ();
			}
		}
	}

	void OnGUI() {
		if (gameRun) {
			string levelLabel = "Level: " + (level + 1);
			string livesLabel = "Lives: " + currentLives;
			string scoreLabel = "Score: " + score;

			GUI.BeginGroup (new Rect (0, 0, Screen.width, Screen.height));
			GUI.Label (new Rect (10, 10, menuImageSide, 40), levelLabel);
			GUI.Label (new Rect (menuImageSide, 10, menuImageSide, 40), livesLabel);
			GUI.Label (new Rect (menuImageSide * 2, 10, menuImageSide, 40), scoreLabel);

			GUI.EndGroup ();
		} else {
			GUI.BeginGroup (new Rect (Screen.width / 2 - 50, Screen.height / 2, menuImageSide, menuImageSide));
			GUI.Label (new Rect (0, 0, menuImageSide, 40), "GAME OVER");
			GUI.Label (new Rect (0, 40, menuImageSide, 40), "Click for restart");
			GUI.EndGroup ();
		}

	}

	public void restartGame(){
		SceneManager.LoadScene("Menu");
	}

	public bool getGameState(){
		return gameRun;
	}

	private void resetAllData(){
		score = 0;
		currentLives = DataManager.lives;
		openItems = 0;
		wave = 0;
		level = ApplicationModel.currentLevel;
	}

	public void addItem(string alienType, int offcet) {
		GameObject prefab = DataManager.GetUfoUnit ();
		float xPos = ((offcet * 1.0f) * DataManager.unitSpace) - DataManager.leftOffcet;
		Vector3 position = new Vector3 (xPos, DataManager.startTopPoint, 0);
		Quaternion rotation = new Quaternion ();
		GameObject ufoObject = Instantiate (prefab, position, rotation);
		Alien alienComponent = ufoObject.GetComponent< Alien >();
		alienComponent.setType (alienType);
		alienComponent.setRow (0);
		alienComponent.setColumn (offcet);
		DataManager.addUnitToList (ufoObject);
	}

	public void resetAliens() {
		for(int i = 0; i < selected.Length; i++) {
			Alien unit = selected[i].GetComponent< Alien >();
			unit.hideHumanoid ();
		}
		if (checkPairs ()) {
			increaseScore ();
			Destroy (selected [0]);
			Destroy (selected [1]);
		};
		selected[1] = null;
		selected[0] = null;
		if (DataManager.getAliensCount () > 2) {
			DataManager.moveAllDown ();
			wave += 1;
		} else {
			nextLevel ();
		}

	}

	public void rotate(){
		int rotateOne = Random.Range (0, 16);
		Debug.Log (rotateOne);
		if (wave >= DataManager.waveCount - 1 && level > 1 & rotateOne > 14 && !isRotated) {
			DataManager.inputsOff ();
			isRotated = true;
		} else {
			if (!isRotated) {
				DataManager.inputsOn ();
			}
		}

	}

	private bool checkPairs() {
		Alien unit1 = selected[0].GetComponent< Alien >();
		Alien unit2 = selected[1].GetComponent< Alien >();
		return unit1.alienType == unit2.alienType;

	}

	public void selectHumanoid(GameObject ufo) {
		selected[openItems] = ufo;
		openItems += 1;
		if (openItems == 2) {
			DataManager.inputsOff ();
			StartCoroutine(MyCoroutine());
		}
	}

	public void nextLevel() {
		wave = 0;
		level += 1;
		if (level < levels.Length) {
			resetLives ();
			DataManager.startNextLevel ();
		} else {
			endGame ();
		}
	}

	public string[] getLevel(){
		return levels[level];
	}

	public int getLevelNum(){
		return level;
	}

	public int getWave() {
		return wave;
	}

	private void endGame(){
		DataManager.gameOver ();
		gameRun = false;
	}

	public void decreaseLives(){
		currentLives--;
		if (currentLives == 0) {
			endGame ();
		}
	}

	private void increaseScore(){
		score += 2;
	}

	public void resetLives(){
		currentLives = DataManager.lives;
	}

	private void initLevels(){
		// level 1
		levels[0] = new string[2];
		levels [0] [0] = "red";
		levels [0] [1] = "green";

		// level 2
		levels[1] = new string[3];
		levels [1] [0] = "red";
		levels [1] [1] = "green";
		levels [1] [2] = "yellow";

		// level 3
		levels[2] = new string[4];
		levels [2] [0] = "red";
		levels [2] [1] = "green";
		levels [2] [2] = "yellow";
		levels [2] [3] = "black";

		// level 4
		levels[3] = new string[5];
		levels [3] [0] = "red";
		levels [3] [1] = "green";
		levels [3] [2] = "yellow";
		levels [3] [3] = "black";
		levels [3] [4] = "violet";



	}

	IEnumerator MyCoroutine()
	{
		yield return new WaitForSeconds(1);  
		resetAliens ();
		openItems = 0;
	}
}
