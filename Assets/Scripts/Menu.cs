﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

using Game;

public class Menu : MonoBehaviour {

	private int menuImageSide, marginButon;

	public GameObject planet;

	public Texture2D forwardArrow, backArrow, start;

	private int level = 0;

	public List<Font> fonts = new List<Font>();

	void Start () {
		menuImageSide = Screen.height / 10;
	}

	void Update () {

	}

	void OnGUI() {
		DrawMenuButton ();
	}

	void startGame(){
		SceneManager.LoadScene("arena");
	}

	void increaseLevel(){
		level += 1;
		ApplicationModel.currentLevel = level;
		Mars planetComp = planet.GetComponent<Mars> ();
		planetComp.setMaterial (level);
	}

	void decreaseLevel(){
		level -= 1;
		ApplicationModel.currentLevel = level;
		Mars planetComp = planet.GetComponent<Mars> ();
		planetComp.setMaterial (level);
	}

	private void DrawMenuButton() {

		float groupLeft = 10;
		float groupTop = 10;

		GUIStyle myStyle = new GUIStyle(); 
		myStyle.alignment = TextAnchor.MiddleCenter; 
		myStyle.normal.textColor = Color.white;

		if (level < 3) {
			GUI.BeginGroup (new Rect (groupLeft, groupTop, menuImageSide, menuImageSide));
			if (GUI.Button (new Rect (0, 0, menuImageSide, menuImageSide), forwardArrow)) {
				increaseLevel ();
			}
			GUI.EndGroup ();
		}

		int absBottom = Screen.height - menuImageSide - 10;

		if (level > 0) {
			
			int absRigth = Screen.width - menuImageSide - 10;


			GUI.BeginGroup (new Rect (absRigth, absBottom, menuImageSide, menuImageSide));
			if (GUI.Button (new Rect (0, 0, menuImageSide, menuImageSide), backArrow)) {
				decreaseLevel ();
			}

			GUI.EndGroup ();
		}

		int startX = Screen.width / 2 - menuImageSide / 2;

		GUI.BeginGroup (new Rect (startX, absBottom, menuImageSide, menuImageSide));
		GUI.skin.label.fontSize = menuImageSide / 3;
		if (GUI.Button (new Rect (0, 0, menuImageSide, menuImageSide / 2), start)) {
			startGame ();
		}

		GUI.EndGroup ();
	}
}
