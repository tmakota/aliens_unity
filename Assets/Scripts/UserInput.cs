﻿using UnityEngine;
using System.Collections;

using Game;

public class UserInput : MonoBehaviour {

	private float timeSinceClickDetected = 0.0f;
	private bool handleClick = false, startedPan = false;
	private Vector3 detectedClickPosition = DataManager.InvalidPosition;
	private Vector3 lastClickPosition = DataManager.InvalidPosition;

	public string errorMessage = "msg";

	private bool blockInput = false;

	void Awake() {
		DataManager.setInputsController (this);
	}

	void Start () {

	}

	void Update () {
		//detect click and schedule HandleClick()
		// - delay the call of this by x milliseconds
		//if click drag is detected before HandleClick() is called do not run it
		ManageCamera();
		if(handleClick) {
			HandleClick();
		}
		timeSinceClickDetected += Time.deltaTime;
	}

	void OnGUI() {
		GUI.BeginGroup(new Rect(0, 0, Screen.width, Screen.height));
		GUI.Label(new Rect(10, Screen.height - 50, Screen.width, 40), errorMessage);
		GUI.EndGroup();
	}

	private void ManageCamera() {
		if (blockInput)
			return;
		if(Input.touchCount > 0 || Input.GetMouseButtonDown(0)) {
			SingleTouch();
		} 
//		else if(Input.touchCount == 2) {
//			PinchZoom();
//		}
	}

	private void SingleTouch() {
		bool detectedClick = false, clickDrag = false;
		Vector3 clickPosition = DataManager.InvalidPosition;
		foreach(Touch touch in Input.touches) {
			if(touch.phase == TouchPhase.Began) {
//				detectedClick = true;
//				clickPosition = new Vector3(touch.position.x, touch.position.y, 0.0f);
			} else if(touch.phase == TouchPhase.Moved || touch.phase == TouchPhase.Stationary) {
//				if(timeSinceClickDetected > 0.9 * DataManager.waitTime) {
//					clickDrag = true;
//					clickPosition = new Vector3(touch.position.x, touch.position.y, 0.0f);
//				}
			} else if(touch.phase == TouchPhase.Ended) {
				detectedClick = true;
				startedPan = false;
				clickDrag = false;
				lastClickPosition = DataManager.InvalidPosition;
			}
		}
		if(Input.GetMouseButtonDown(0)) {
			detectedClick = true;
			clickPosition = Input.mousePosition;
		}
		if(clickPosition == DataManager.InvalidPosition) return;
		if(detectedClick) {
			handleClick = true;
			detectedClickPosition = clickPosition;
//			timeSinceClickDetected = 0.0f;
		} else if(clickDrag) {
//			handleClick = false;
//			detectedClickPosition = DataManager.InvalidPosition;
		}
		if(clickDrag) {
//			Vector2 screenPosition = new Vector2(clickPosition.x, Screen.height - clickPosition.y);
//			bool movingCamera = false;
//			Vector3 movement = new Vector3(0, 0, 0);
//			if(DataManager.leftZone.Contains(screenPosition)) {
//				movement.x -= DataManager.panSpeed;
//				movingCamera = true;
//			}
//			if(DataManager.rightZone.Contains(screenPosition)) {
//				movement.x += DataManager.panSpeed;
//				movingCamera = true;
//			}
//			if(DataManager.topZone.Contains(screenPosition)) {
//				movement.z += DataManager.panSpeed;
//				movingCamera = true;
//			}
//			if(DataManager.bottomZone.Contains(screenPosition)) {
//				movement.z -= DataManager.panSpeed;
//				movingCamera = true;
//			}
//			if(movingCamera) {
//				Vector3 origin = Camera.main.transform.position;
//				Vector3 destination = origin;
//				movement = Camera.main.transform.TransformDirection(movement);
//				destination.x += movement.x;
//				destination.z += movement.z;
//				Camera.main.transform.position = Vector3.MoveTowards(origin, destination, DataManager.panSpeed * Time.deltaTime);
//				return;
//			}
			if(startedPan) { //rotating camera
//				float xDrag = clickPosition.x - lastClickPosition.x;
//				transform.Rotate(Vector3.up, xDrag * DataManager.rotateSpeed * Time.deltaTime);
//				transform.RotateAround(Camera.main.transform.position, Vector3.up, xDrag * Resources.rotateSpeed * Time.deltaTime);
//				lastClickPosition = clickPosition;
			} else {
				if(lastClickPosition == DataManager.InvalidPosition) {
					lastClickPosition = clickPosition;
					startedPan = true;
				}
			}
		}
	}

	private void HandleClick() {
		if(detectedClickPosition == DataManager.InvalidPosition) return;
		GameObject hitObject = FindHitObject(detectedClickPosition);
//		Vector3 hitPoint = FindHitPoint(detectedClickPosition);
		sheep atacker = hitObject.transform.parent.GetComponent<sheep>();
		errorMessage = "click";
		if (DataManager.isGameRuning()) {
			if (atacker) {
				errorMessage = "click on atacker";
				Alien unit = hitObject.transform.parent.transform.parent.GetComponent< Alien > ();
				if (unit && !unit.selected) {
					errorMessage = "click on unit";
					unit.handleClick ();
				}
			}
		} else {
			DataManager.restart ();
		}

		handleClick = false;
//		if(gameManager) gameManager.HandleClick(hitObject);
	}

	private GameObject FindHitObject(Vector3 screenPosition) {
		Ray ray = Camera.main.ScreenPointToRay(screenPosition);
		RaycastHit hit;
		if(Physics.Raycast(ray, out hit)) return hit.collider.gameObject;
		else return null;
	}

	private Vector3 FindHitPoint(Vector3 screenPosition) {
		Ray ray = Camera.main.ScreenPointToRay(screenPosition);
		RaycastHit hit;
		if(Physics.Raycast(ray, out hit)) return hit.point;
		else return DataManager.InvalidPosition;
	}

	public void releaseInput() {
		blockInput = false;
	}

	public void disableInput() {
		blockInput = true;
	}
}
