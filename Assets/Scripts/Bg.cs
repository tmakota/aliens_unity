﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Game;

public class Bg : MonoBehaviour {

	public Material[] matrials;
	private Renderer sky;

	// Use this for initialization
	void Awake(){
		DataManager.SetSky (this);
		sky = GetComponent<Renderer>();
	}

	// Use this for initialization
	void Start () {
		setMaterial (ApplicationModel.currentLevel);
	}

	public void setMaterial(int indexMat) {
		sky.material = matrials[indexMat];
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
